
function spawn_once(cmd, options)
	if not options then options = {} end
	local matcher = ""
	if options.match == nil then
    matcher = string.match(string.match(cmd, "^%S*"), "[^/]-$") or cmd
	elseif options.match == true then
		matcher = "-f '" .. cmd .. "'"
	else
		matcher = options.match
	end
	local command = "pgrep " .. matcher .. " >/dev/null || (" .. cmd .. ")"
	awful.spawn.with_shell(command)
end


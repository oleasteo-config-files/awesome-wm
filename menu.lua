menu_awesome = {
  { "hotkeys",     function() return false, hotkeys_popup.show_help end},
  { "manual",      terminal_exec .. "man awesome" },
  { "restart",     awesome.restart                },
  { "quit",        awesome.quit                   }
}

menu_system = {
  { "lock",           "xlock"                  },
  { "suspend",        "systemctl suspend"      },
  { "lock & suspend", function () awful.spawn.with_shell("sleep 0.5 && systemctl suspend & xlock") end },
--  { "hybrid-sleep",   "systemctl hybrid-sleep" },
--  { "hibernate",      "systemctl hibernate"    },
  {},
  { "reboot",         "systemctl reboot"       },
  { "poweroff",       "systemctl poweroff"     }
}

menu_main = {
  { "awesome",       menu_awesome },
  { "open terminal", terminal     },
  { "system",        menu_system  }
}

menu = awful.menu({
  items = menu_main
})